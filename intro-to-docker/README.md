# intro-to-docker

Minimal notes/slides for an intro to Docker lightning talk

## Key points

* What?
  * VMs
  * Containers
    * Single process (recommended, but not required)
  * Docker
* Why?
  * Portable
  * Standard
  * Small/isolated
* Concepts
  * Image
  * Container
  * Registry
    * hub.docker.com
  * Tag
* Local development
  * Pull an image - `docker pull hello-world`
  * Run an image - `docker run hello-world` -- `docker run docker/whalesay cowsay "message"`
  * Expose a port - `docker run -p 8080:8080 whitlaaa/healthz-demo`
  * Build an image - `docker build -t watch-demo .`
  * Mount a volume for local development - `docker run -it -v $(PWD):/app -p 3000:3000 -p 35729:35729 watch-demo`
* Dockerfile
  * FROM
  * COPY
  * RUN
  * ENV
* Docker Compose
  * Multiple services
  * Shared network
