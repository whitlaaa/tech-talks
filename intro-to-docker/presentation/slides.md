![docker-logo](/presentation/images/docker-logo.png)

---

## VMs vs Containers

* Virtual machines virtualize from hardware up
* Containers virtualize from the OS up

---

## Docker

* Portability
* Isolation
* User-friendly

--

### Some quick terminology

* **Image** - immutable definition of a process (e.g., `msyql`)
* **Container** - a running (usually customized) instance of an image
* **Registry** - storage location for images (e.g., [Docker Hub](https://hub.docker.com))
* **Tag** - a specific version of an image (e.g., `mysql:8` or `golang:latest`)
